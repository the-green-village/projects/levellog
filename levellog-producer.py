# -*- coding: utf-8 -*-
"""
Connector between the LevelLog API and The Green Village digital platform.

Retrieves LevelLog data via API and sends it to Kafka using a Kafka producer.
Each measurement is sent as a separate Kafka message.
"""

import logging
import pathlib
import json
import http.client
from configparser import ConfigParser
from datetime import datetime, timedelta
import time

import requests
from tgvfunctions import tgvfunctions

# Relative imports
from projectsecrets import projectsecrets
import config

logging.basicConfig(
    format="%(asctime)s - %(funcName)s:%(lineno)s - %(levelname)s - %(message)s", level=logging.INFO    
)
logger = logging.getLogger(__file__)


def main():
    # Initalizing Kafka
    logging.debug("Initalizing KAFKA")
    topic = projectsecrets["TOPIC"]
    tgv = tgvfunctions.TGVFunctions(topic)

    producer_name = config.config["producer_name"]
    producer = tgv.make_producer(producer_name)
    # Fetch authentication token
    levellog_client_id = projectsecrets["LEVELLOG_client_id"]
    levellog_client_secret = projectsecrets["LEVELLOG_client_secret"]

    # Fetch remaining config setup
    desired_frequency = config.config["desired_frequency"]
    desired_time = 1/desired_frequency
    logger.info(f"Desired frequency: {desired_frequency} Hz, desired time: {desired_time} s")
    
    # Optional: on_delivery handler (triggered by poll() or flush())
    # when a message has been successfully delivered or failed to deliver (after retries).
    def acked(err, msg):
        global delivered_records
        """Delivery report handler called on
        successful or failed delivery of message
        """
        if err is not None:
            print("Failed to deliver message: {}".format(err))
        else:
            delivered_records += 1
            print(
                "Produced record to topic {} partition [{}] @ offset {}".format(
                    msg.topic(), msg.partition(), msg.offset()
                )
            )
    # Fetch API token
    payload = f"grant_type=client_credentials&client_id={levellog_client_id}&client_secret={levellog_client_secret}&scope=https://api.levellog.nl/.default"
    headers = {"Content-Type": "application/x-www-form-urlencoded"}
    r = requests.post(
        "https://login.microsoftonline.com/19f18e9e-d5fe-4cef-b8e3-7c05d866ed9d/oauth2/v2.0/token",
        headers=headers,
        data=payload,
    )
    logger.debug(r.text)
    token = r.json()["access_token"]
    # Fetch LevelLog devices
    headers = {"accept": "application/json", "Authorization": "Bearer " + token}
    r = requests.get(
        "https://levellog.azurewebsites.net/api/v1/levellogs", headers=headers
    )
    logger.debug(r.text)
    levellogs = r.json()["LevelLogs"]
    
    startdate = datetime.strftime(datetime.now() - timedelta(2), "%Y-%m-%d")
    enddate = datetime.strftime(datetime.now(), "%Y-%m-%d")
    # startdate = "2020-01-01"
    # enddate = "2020-01-31"

    params = {"startdate": startdate, "enddate": enddate, "rows": 1000, "skip": 0}
    try: 
        for levellog in levellogs:
            if not levellog["Code"] in [
                #"46de764d5bdae4fe",#PB01 2022-11-19T16:22:52.443
                "3a5e569882051bf6",#PB02 2025-03-05T14:44:12.047
                "332ec5d3e79b9588",#PB03 2025-03-07T08:47:46.85
                #0388373c04be4a8a # PB01 2023-09-11T16:20:56.357
                #96b4ac89cb1062a7 #PB04 2023-11-13T14:39:56.613
                #"96b4ac89cb1062a7" #PB01 2024-12-02T14:48:39.003 Deze hoort te werken doet dat nietS
                #b5754ba61af94b4c #PB03 2021-11-01T08:23:00.347
                #96b4ac89cb1062a7 #PB02 2021-11-01T14:38:26.123
                #99949b51f9a41c7a #Watermolen01 2021-04-28T05:03:21.537
                #12a9098768678417 #PB01 2021-11-01T16:40:10.267
                #fa1537c3134c1275 #Watermolen01 2025-03-06T17:07:29.64

            ]:
                logger.debug(f"Skipping device: {levellog['Code']}")
                continue
            # Fetch measurement data
            logger.debug(f"Fetching data for device: {levellog['Code']}")
            r = requests.get(
                f"https://levellog.azurewebsites.net/api/v1/levellogs/{levellog['Code']}/measures",
                headers=headers,
                params=params,
            )
            logger.debug(f"Response: {r.text}, Status: {r.status_code}")

            if not r.status_code == 200:
                logger.warning(f"Failed to fetch data for device: {levellog['Code']}")
                continue

            measures = r.json()["measures"]

            for measure in measures:
                for measurement in [
                    {
                        "measurement_id": "TubeLevel",
                        "value": measure["TubeLevel"],
                        "unit": "mm",
                    },
                    {
                        "measurement_id": "GroundLevel",
                        "value": measure["GroundLevel"],
                        "unit": "mm",
                    },
                    {
                        "measurement_id": "Temperature",
                        "value": measure["Temperature"],
                        "unit": "°C",
                    },
                    {
                        "measurement_id": "Is Valid",
                        "value": measure["Is Valid"],
                    },
                    {
                        "measurement_id": "Retries",
                        "value": measure["Retries"],
                    },
                    {
                        "measurement_id": "RefTube",
                        "value": measure["RefTube"],
                        "unit": "mm",
                    },
                    {
                        "measurement_id": "RefGround",
                        "value": measure["RefGround"],
                        "unit": "mm",
                    },
                ]:
                    kafka_message = tgvfunctions.generate_kafka_message(
                        project_id = "levellog",
                        application_id = "LevelLog",
                        device_id = measure["Code"],
                        device_description = levellog["Identification"],
                        timestamp=int(time.time() * 1e3), # Millisecond timestamp
                        measurements=[measurement],
                        latitude = levellog["Latitude"],
                        longitude =  levellog["Longitude"]
                        )
                    logger.debug(f"Producing Kafka message: {kafka_message}")

                    tgv.produce_fast(producer,kafka_message)
    except Exception as e:
        logger.warning(f"An error occured: {e}")
    finally:
        producer.flush()
        logger.info(f"Finished producing messages to topic {topic}.")


if __name__ == "__main__":
    main()
