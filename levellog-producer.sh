#!/bin/bash
source ~/producers/.kafka_envs

cd ~/producers/levellog
. env/bin/activate
python levellog-producer.py
