# LevelLog

https://levellog.azurewebsites.net/api/openapi/ui

The sensor data is collected on hourly basis and it is transmitted from the devices on daily basis.

```
# Obtain a bearer token (valid for 1 hour)
curl -X POST -H 'Content-Type: application/x-www-form-urlencoded' -d 'grant_type=client_credentials&client_id=<client-id>&client_secret=<application-secret>&scope=https://api.levellog.nl/.default' https://login.microsoftonline.com/19f18e9e-d5fe-4cef-b8e3-7c05d866ed9d/oauth2/v2.0/token

curl -s -X GET -H "accept: application/json" -H "Authorization: Bearer $TOKEN" \
  "https://levellog.azurewebsites.net/api/v1/levellogs" | jq .

curl -s -X GET -H "accept: application/json" -H "Authorization: Bearer $TOKEN" \
  "https://levellog.azurewebsites.net/api/v1/levellogs/12a9098768678417" | jq .

curl -s -X GET -H "accept: application/json" -H "Authorization: Bearer $TOKEN" \
  "https://levellog.azurewebsites.net/api/v1/levellogs/12a9098768678417/measures?startdate=2020-01-01&enddate=2020-02-01&rows=1000&skip=0" | jq .
```

```
python -m venv venv
. venv/bin/activate

pip install -U -r requirements.txt
python levellog-producer.py
```

## Virtual Machine

The Kafka producer runs as a cronjob on the Virtual Machine of The Green Village.

Define a cron job to read data periodically.

```
crontab -e
```

```
0 12 * * * /usr/bin/python3 /home/jamvanderweijd/levellog/levellog-producer.py > /home/jamvanderweijd/levellog/cronjob.log 2>&1
```
